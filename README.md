Simple implementation of huffman encoding

It uses the UTF8-CPP lib described in http://utfcpp.sourceforge.net/


To use compile with g++ in standar C++11

For create the encoder executable just run

  g++ -o myname_encoder huffman_main.cpp -std=c++11

For create the decoder executable run instead

  g++ -o myname_encoder huffman_main.cpp -std=c++11 -D DECODE


If you want to see the required arguments, just run the executable without arguments
the executable prints the arguments in order.
