

#ifndef TEXT_ANALIZER_H
#define TEXT_ANALIZER_H

#include "utf8.h"
#include <fstream>
#include <iostream>
#include <exception>
#include <string>
#include <vector>
#include <map>


class TextStadistics{
private:
    std::map<uint32_t, long> count; //code point utf8 | frecuency
    std::map<char, long> count_byte;
    std::ifstream fs8;
    std::ifstream ifs;
public:
    TextStadistics(){
    }

    static std::string utf8chr(int cp){
        char c[5]={ 0x00,0x00,0x00,0x00,0x00 };
        if     (cp<=0x7F) { c[0] = cp;  }
        else if(cp<=0x7FF) { c[0] = (cp>>6)+192; c[1] = (cp&63)+128; }
        else if(0xd800<=cp && cp<=0xdfff) {} //invalid block of utf8
        else if(cp<=0xFFFF) { c[0] = (cp>>12)+224; c[1]= ((cp>>6)&63)+128; c[2]=(cp&63)+128; }
        else if(cp<=0x10FFFF) { c[0] = (cp>>18)+240; c[1] = ((cp>>12)&63)+128; c[2] = ((cp>>6)&63)+128; c[3]=(cp&63)+128; }
        return std::string(c);
    }

    static int codepoint(const std::string &u){
        int l = u.length();
        if (l<1) return -1; unsigned char u0 = u[0]; if (u0>=0   && u0<=127) return u0;
        if (l<2) return -1; unsigned char u1 = u[1]; if (u0>=192 && u0<=223) return (u0-192)*64 + (u1-128);
        if (u[0]==0xed && (u[1] & 0xa0) == 0xa0) return -1; //code points, 0xd800 to 0xdfff
        if (l<3) return -1; unsigned char u2 = u[2]; if (u0>=224 && u0<=239) return (u0-224)*4096 + (u1-128)*64 + (u2-128);
        if (l<4) return -1; unsigned char u3 = u[3]; if (u0>=240 && u0<=247) return (u0-240)*262144 + (u1-128)*4096 + (u2-128)*64 + (u3-128);
        return -1;
    }

    static std::vector<unsigned char> uni2utf8(uint32_t unicode){
        uint32_t hib = unicode & 0x000000FF;
        std::vector<unsigned char> v;
        if(hib > 0x000000F0){ // four byte char
            //std::cout << "four byte ";
            char u8[4];
            utf8::append(unicode, u8);
            for(int i = 0; i < 4; i++){
              v.push_back(u8[i]);
            }
        }
        else if(hib > 0x000000E0){ // three byte
            //std::cout << "three byte ";
            char u8[3];
            utf8::append(unicode, u8);
            for(int i = 0; i < 3; i++){
              v.push_back(u8[i]);
            }
        }
        else if(hib > 0x000000C0){ // two byte
            //std::cout << "two byte ";
            char u8[2];
            utf8::append(unicode, u8);
            for(int i = 0; i < 2 ; i++){
              v.push_back(u8[i]);
            }
        }
        else if(hib > 0x00000080){ // error

        }
        else{ // one  byte
            //std::cout << "one byte ";
            char u8[1];
            utf8::append(unicode, u8);
            v.push_back(u8[0]);
        }
        return v;
    }

    void makeCountUtf8(std::string filename){
        fs8.open(filename);
        //std::ofstream test;
        //test.open("utf8_test.txt", std::ios::binary);
        if(fs8.is_open()){
            count.clear();
            std::string line;
            std::map<uint32_t, long>::iterator it;
            //std::string enc_out;
            //std::string::iterator enit = enc_out.begin();
            //std::ofstream out;
            count[0x0d0a] = 0;
            while (getline(fs8, line)) {
                std::string::iterator begin = line.begin();
                std::string::iterator end = line.end();
                //for(int i = 0; i < line.size(); i++){
                //  out << line[i];
                //}
                //
                uint32_t code_p;
                while(begin != end){
                    code_p = utf8::next(begin, end);
                    //test << utf8chr(code_p);
                    //if(code_p == 9) std::cout << "tab" << std::endl;
                    //std::cout << code_p << " ";
                    it = count.find(code_p);
                    if(it == count.end()){
                        count[code_p] = 1;
                    }
                    else{
                        count[code_p] = count[code_p] + 1;
                    }
                }
                count[0x0d0a] = count[0x0d0a] + 1;
                //test << "\n";
            }
            //test.close();
            fs8.close();
        }
    }

    void makeCountByteArray(std::string filename){
        ifs.open(filename, std::ios::binary|std::ios::ate);
        std::vector<char> result;
        if(ifs.is_open()){
            count_byte.clear();
            std::ifstream::pos_type pos = ifs.tellg();

            result = std::vector<char> (pos);

            ifs.seekg(0, std::ios::beg);
            ifs.read(&result[0], pos);
            std::map<char, long>::iterator it;
            char temp;
            for(int i = 0; i<result.size(); i++){
                temp = result[i];
                it = count_byte.find(temp);
                if(it == count_byte.end()){
                    count_byte[temp] = 1;
                }
                else{
                    count_byte[temp] = count_byte[temp] + 1;
                }
            }
            ifs.close();
        }
    }

    void exportMapUtf8CSV(std::string filename){
        std::ofstream count_file;
        count_file.open(filename, std::ios_base::binary);
        std::string aux;
        int cp;
        for (std::map<uint32_t, long>::iterator it=count.begin(); it!=count.end(); ++it){
            //std::cout << (char32_t)it->first << " => " << it->second << '\n';
            if(it->first != 0x0d0a){
              count_file << utf8chr(codepoint("'")) << utf8chr(it->first) << utf8chr(codepoint("'")) << utf8chr(codepoint(","));
              aux = std::to_string(it->second);
              count_file << aux;
            }
            else{
              count_file << utf8chr(codepoint("'")) << "\\n" << utf8chr(codepoint("'")) << utf8chr(codepoint(","));
              aux = std::to_string(it->second);
              count_file << aux;
            }
            count_file << '\n';
        }
        count_file.close();
    }

    void exportMapUniCSV(std::string filename){
      std::ofstream count_file;
      count_file.open(filename, std::ios::binary);
      std::string aux;
      for (std::map<uint32_t, long>::iterator it=count.begin(); it!=count.end(); ++it){
          count_file << std::to_string(it->first) << "," << std::to_string(it->second) << "\n";
      }
      count_file.close();
    }

    void exportMapByteArray(std::string filename){
        std::ofstream count_file;
        count_file.open(filename, std::ios::binary);
        std::string aux;
        int cp;
        for (std::map<char, long>::iterator it=count_byte.begin(); it!=count_byte.end(); ++it){
            //std::cout << (char32_t)it->first << " => " << it->second << '\n';
            count_file << utf8chr(it->first) << ",";
            aux = std::to_string(it->second);
            count_file << aux;
            count_file << '\n';
        }
        count_file.close();
    }

    std::map<uint32_t, long> getMapCountUni(){
        return count;
    }
};

#endif
