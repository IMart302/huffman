#ifndef BNODE_H
#define BNODE_H

template <class T>
class BNode{
private:
  BNode<T> *left;
  BNode<T> *right;
  BNode<T> *parent;
  T data;
public:
  BNode(){
    left = nullptr;
    right = nullptr;
    parent = nullptr;
  }

  virtual ~BNode(){
    //release();
  }

  BNode(T data, BNode<T>* left, BNode<T>* right, BNode<T>* parent = nullptr){
    this->data = data;
    this->left = left;
    this->right = right;
    this->parent = parent;
  }

  //template <class T>
  BNode(T data){
    this->data = data;
    this->left = nullptr;
    this->right = nullptr;
    this->parent = nullptr;
  }

  //template <class T>
  T get(){ //Get root data
    return data;
  }

  BNode<T>* getLeft(){
    //template <class T>
    return left;
  }

  //template <class T>
  BNode<T>* getRight(){
    return right;
  }

  BNode<T>* getParent(){
    return parent;
  }

  void setLeft(BNode<T>* left){
    this->left = left;
  }

  void setRight(BNode<T>* right){
    this->right = right;
  }

  void setParent(BNode<T>* parent){
    this->parent = parent;
  }

  //template <class T>
  bool operator < (const BNode<T> &b)const {
    return (this->data < b.data);
  }

  //template <class T>
  bool operator > (const BNode<T> &b) const{
    return (this->data > b.data);
  }

  //template <class T>
  bool operator <= (const BNode<T> &b) const{
    return (this->data <= b.data);
  }

  //template <class T>
  bool operator >= (const BNode<T> &b) const{
    return (this->data >= b.data);
  }

  static BNode<T>* merge(BNode<T>* b1, BNode<T>* b2){
    BNode<T>* m = new BNode<T>(b1->get() + b2->get(), b1, b2);
    b1->setParent(m);
    b2->setParent(m);
    return m;
  }

  static BNode<T>* construct(std::vector<T> &inorder, std::vector<T> &preorder){
    return construct_internal(inorder, 0, inorder.size()-1, preorder, 0, preorder.size()-1);
  }

  static BNode<T>* construct_internal(std::vector<T> &inorder, int instart, int inend, std::vector<T> &preorder, int prestart, int preend){
    if(prestart > preend || instart > inend){
      return nullptr;
    }

    T val = preorder[prestart];
    BNode<T>* b = new BNode<T>(val);

    int k = 0;
    for(int i = 0; i<inorder.size(); i++){
      if(val == inorder[i]){
        k = i;
        i = inorder.size();
      }
    }

    b->setLeft(BNode<T>::construct_internal(inorder, instart, k-1, preorder, prestart+1, prestart+(k-instart)));
    b->setRight(BNode<T>::construct_internal(inorder, k+1, inend, preorder, prestart+(k-instart)+1, preend));
    if(b->getLeft() != nullptr)
      b->getLeft()->setParent(b);
    if(b->getRight() != nullptr)
      b->getRight()->setParent(b);
    return b;
  }

  void inorder(std::vector<T> &in_out){
    inorder_internal(this, in_out);
  }

  void inorder_internal(BNode<T>* root, std::vector<T> &in_out){ //left, root, right
    if(root->getLeft() != nullptr){
      inorder_internal(root->getLeft(), in_out);
    }
    in_out.push_back(root->get());
    if(root->getRight() != nullptr){
      inorder_internal(root->getRight(), in_out);
    }
  }

  void preorder(std::vector<T> &pre_out){
    preorder_internal(this, pre_out);
  }

  void preorder_internal(BNode<T>* root, std::vector<T> &pre_out){ //root, left, right
    pre_out.push_back(root->get());
    if(root->getLeft() != nullptr){
      preorder_internal(root->getLeft(), pre_out);
    }
    if(root->getRight() != nullptr){
      preorder_internal(root->getRight(), pre_out);
    }
  }

  void release(){
    if(this->left != nullptr){
      this->left->release();
      delete left;
    }
    if(this->right != nullptr){
      this->right->release();
      delete right;
    }
  }
};

#endif
