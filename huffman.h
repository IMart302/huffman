#ifndef HUFFMAN_H
#define HUFFMAN_H

#include <queue>
#include <map>
#include <bitset>
#include <vector>
#include <string>
#include <fstream>
#include "text_analizer.h"
#include "bnode.h"
#include "utf8.h"
#include "bitset_adapter.h"

template <class T>
class BNodeComparator{
public:
  bool operator()(const BNode<T>* b1, const BNode<T>* b2) const{
    return (*b1 > *b2);
  }
};

class FreqPair{
private:
  std::vector<uint32_t> uni;
  long freq;
public:
  FreqPair(){
    freq = 0;
  }

  FreqPair(std::vector<uint32_t> unis, long freq){
    uni = unis;
    this->freq = freq;
  }

  bool operator <(const FreqPair &p) const{
    return (this->freq < p.freq);
  }

  bool operator >(const FreqPair &p) const{
    return (this->freq > p.freq);
  }

  uint32_t checkFirst(){
    if(!uni.empty())
      return uni.front();
    else
      return 0;
  }

  bool operator ==(const FreqPair &p) const{
    if(this->uni.size() != p.uni.size())
      return false;

    //std::vector<uint32_t>::iterator it=this->uni.begin();
    //std::vector<uint32_t>::iterator it2=p.uni.begin();
    //bool equal = true;
    //while(it!=this->uni.end() && it2 != p.uni.end() && equal){
    //  if(*it != *it2) equal = false;
    //  ++it;
    //  ++it2;
    //}
    //if(!equal) return false;
    for(int i = 0; i<this->uni.size(); i++){
      if(this->uni[i] != p.uni[i])
        return false;
    }

    if(this->freq != p.freq)
      return false;

    return true;
  }

  FreqPair operator +(const FreqPair &p) const{
    std::vector<uint32_t> conc;
    conc.reserve(this->uni.size() + p.uni.size());
    conc.insert(conc.end(), this->uni.begin(), this->uni.end());
    std::vector<uint32_t> sec = p.uni;
    conc.insert(conc.end(), sec.begin(), sec.end());
    long nfr = this->freq + p.freq;
    return FreqPair(conc, nfr);
  }

  void operator =(const FreqPair &f){
    this->uni.clear();
    for(int i=0; i<f.uni.size(); i++){
        this->uni.push_back(f.uni[i]);
    }
    this->freq = f.freq;
  }

  bool operator()(FreqPair &p1, FreqPair &p2){
    return p1.second() < p2.second();
  }

  std::vector<uint32_t> first(){
    return uni;
  }

  long second(){
    return freq;
  }
};

class Huffman{
private:
  BitsetWrapper encoded;
  BNode<FreqPair>* solver_tree_root;
  long enc_size;
public:
  Huffman(){
    solver_tree_root = nullptr;
    enc_size = 0;
  }

  void huffmanEncode(std::string filename){
    TextStadistics st;
    st.makeCountUtf8(filename);
    std::map<uint32_t, long> count = st.getMapCountUni();
    std::priority_queue<BNode<FreqPair>*, std::vector<BNode<FreqPair>*>, BNodeComparator<FreqPair>> p_queue;
    std::vector<BNode<FreqPair>*> leaf_list;
    BNode<FreqPair>* leaf;
    //std::cout << "Initializing tree \n";
    for (std::map<uint32_t, long>::iterator it=count.begin(); it!=count.end(); ++it){
      std::vector<uint32_t> v;
      v.push_back(it->first);
      leaf = new BNode<FreqPair>(FreqPair(v, it->second));
      p_queue.push(leaf);
      leaf_list.push_back(leaf);
    }
    //std::cout << "Making tree \n";
    BNode<FreqPair>* node1;
    BNode<FreqPair>* node2;
    BNode<FreqPair>* newnode;
    while(p_queue.size()>1){
      node1 = p_queue.top();
      p_queue.pop();
      node2 = p_queue.top();
      p_queue.pop();
      newnode = BNode<FreqPair>::merge(node1, node2);
      p_queue.push(newnode);
    }

    solver_tree_root = newnode;

    //std::cout << "Encoding file \n";
    //Encoding file;
    std::ifstream fs8;
    fs8.open(filename);
    std::string line;
    uint32_t codepoint;
    BNode<FreqPair>* leaf_ptr = nullptr;
    BNode<FreqPair>* act_parent = nullptr;
    BitsetWrapper prev;
    BitsetWrapper rev;
    std::string::iterator begin;
    std::string::iterator end;
    int i;
    if(fs8.is_open()){
      encoded.clear();
      while(getline(fs8, line)){
        begin = line.begin();
        end = line.end();
        while(begin != end){
          codepoint = utf8::next(begin, end);
          //std::cout << codepoint << "\n";
          for(i = 0; i<leaf_list.size(); i++){
            if(leaf_list[i]->get().checkFirst() == codepoint){
              leaf_ptr = leaf_list[i];
              i = leaf_list.size();
            }
          }
          if(leaf_ptr != nullptr){
            prev.clear();
            act_parent = leaf_ptr->getParent();
            while(act_parent != nullptr){
              if(act_parent->getRight() == leaf_ptr){
                prev.pushBit(1);
                //std::cout << "l";
              }
              else if(act_parent->getLeft() == leaf_ptr){
                prev.pushBit(0);
                //std::cout << "0";
              }
              leaf_ptr = act_parent;
              act_parent = leaf_ptr->getParent();
            }
            //std::cout << "\n";
            prev.reverse(rev);
            encoded.concatenate(rev);
            leaf_ptr =  nullptr;
          }
        }
        for(i = 0; i<leaf_list.size(); i++){
          if(leaf_list[i]->get().checkFirst() == 0x0d0a){
            leaf_ptr = leaf_list[i];
            i = leaf_list.size();
          }
        }
        if(leaf_ptr != nullptr){
          prev.clear();
          act_parent = leaf_ptr->getParent();
          while(act_parent != nullptr){
            if(act_parent->getRight() == leaf_ptr){
              prev.pushBit(1);
            }
            else if(act_parent->getLeft() == leaf_ptr){
              prev.pushBit(0);
            }
            leaf_ptr = act_parent;
            act_parent = leaf_ptr->getParent();
          }
          prev.reverse(rev);
          encoded.concatenate(rev);
          leaf_ptr =  nullptr;
        }
      }
    }
    //std::cout << "Done \n";
    enc_size = encoded.getCount();
  }

  void exportEncodingHex(std::string filename){
    encoded.exportToHexFile(filename);
  }

  void exportSolverFile(std::string filename){
    std::ofstream of;
    of.open(filename+".txt");
    if(!of.is_open())
      return;

    std::vector<FreqPair> inorder;
    std::vector<FreqPair> preorder;
    solver_tree_root->inorder(inorder);
    solver_tree_root->preorder(preorder);

    of<<"enc_size=\n";
    of<<enc_size<<"\n";
    of<<"inorder=\n";
    std::vector<uint32_t> unis;
    FreqPair temp;
    for(int i = 0; i < inorder.size(); i++){
      temp = inorder[i];
      unis = temp.first();
      for(int j = 0; j<unis.size()-1; j++){
        of<<unis[j]<<",";
      }
      of<<unis[unis.size()-1]<<":"<<temp.second()<<"\n";
    }
    of<<"preorder=\n";
    for(int i = 0; i < preorder.size(); i++){
      temp = preorder[i];
      unis = temp.first();
      for(int j = 0; j<unis.size()-1; j++){
        of<<unis[j]<<",";
      }
      of<<unis[unis.size()-1]<<":"<<temp.second()<<"\n";
    }
    of.close();
  }

  void importSolverFile(std::string filename){
    std::ifstream ifs;
    ifs.open(filename);
    if(!ifs.is_open()) return;

    std::vector<std::string> lines;
    std::string line;
    while(getline(ifs, line)){
      if(!line.empty())
        lines.push_back(line);
    }
    ifs.close();

    if(lines[0] == "enc_size=")
      enc_size = std::stol(lines[1]);

    int l = 3;
    std::vector<FreqPair> inorder;
    std::vector<FreqPair> preorder;
    std::vector<uint32_t> unis;
    std::string node_str;
    std::string number;
    if(lines[2] == "inorder="){
      node_str = lines[l];
      while(node_str != "preorder=" && l < lines.size()){
        int i=0;
        while(node_str[i] != ':'){
          if(node_str[i] == ','){
            unis.push_back((uint32_t)std::stoi(number));
            number.clear();
          }
          else{
            number.push_back(node_str[i]);
          }
          i++;
        }
        unis.push_back((uint32_t)std::stoi(number));
        i++;
        number.clear();
        while(i < node_str.size()){
          number.push_back(node_str[i]);
          i++;
        }
        long freq = std::stol(number);
        number.clear();
        inorder.push_back(FreqPair(unis, freq));
        unis.clear();
        l+=1;
        node_str = lines[l];
      }
    }
    l++;
    while(l < lines.size()){
      node_str = lines[l];
      int i=0;
      while(node_str[i] != ':'){
        if(node_str[i] == ','){
          unis.push_back((uint32_t)std::stoul(number));
          number.clear();
        }
        else{
          number.push_back(node_str[i]);
        }
        i++;
      }
      unis.push_back((uint32_t)std::stoul(number));
      i++;
      number.clear();
      while(i < node_str.size()){
        number.push_back(node_str[i]);
        i++;
      }
      long freq = std::stol(number);
      number.clear();
      preorder.push_back(FreqPair(unis, freq));
      unis.clear();
      l+=1;
    }
    if(solver_tree_root != nullptr){
      solver_tree_root->release();
    }
    solver_tree_root = BNode<FreqPair>::construct(inorder, preorder);
  }

  void importEncodedFile(std::string filename){
    encoded.importFromHexFile(filename);
  }

  void release(){
    encoded.clear();
    solver_tree_root->release();
    delete solver_tree_root;
    solver_tree_root = nullptr;
  }

  void huffmanDecode(std::string filename){
    if(solver_tree_root == nullptr) return;
    if(encoded.getCount() == 0) return;
    std::ofstream ofs;
    ofs.open(filename+".txt", std::ios_base::binary);
    if(!ofs.is_open()) return;

    BNode<FreqPair>* act_parent = solver_tree_root;
    BNode<FreqPair>* child;
    uint32_t codepoint;
    long count = 0;
    //std::cout << "decoding" << std::endl;
    short bit;
    while(count < enc_size){
      //std::cout << "decoding" << std::endl;

      bit = encoded.getBitLeast(count);
      //std::cout << bit ;
      if(bit == 0){
        child = act_parent->getLeft();
      }
      else if(bit == 1){
        child = act_parent->getRight();
      }
      if(child == nullptr){
        codepoint = act_parent->get().first()[0];
        //std::cout <<" "<< codepoint << " \n ";
        if(codepoint == 0x0d0a){
          ofs << "\n";
        }
        else{
          ofs << TextStadistics::utf8chr(codepoint);
        }
        act_parent = solver_tree_root;
        count--;
      }
      else{
        act_parent = child;
      }
      count+=1;
    }
    ofs.close();
  }
};

#endif
