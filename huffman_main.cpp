#include <iostream>
#include <string>
#include <fstream>
#include <exception>
#include <vector>
#include "huffman.h"


#ifdef DECODE

int main(int argc, char** argv){

#ifdef PIPE_STDOUT
  if (argc != 3) {
      std::cerr << "\nUsage: arg1->file to decode; arg2->solver file \n";
      return 0;
  }
  Huffman h;
  h.importSolverFile(std::string(argv[2]));
  h.importEncodedFile(std::string(argv[1]));
  h.huffmanDecode("huffman_temp");
  h.release();

  std::ifstream ifs;
  ifs.open(std::string("huffman_temp.txt"));
  if(ifs.is_open()){
    char c;
    while (ifs.get(c)) {
      std::cout << c;
    }
  }
#else
  if (argc != 4) {
      std::cerr << "\nUsage: arg1->file to decode; arg2->solver file, arg3 -> output name(without extension) \n";
      return 0;
  }
  Huffman h;
  h.importSolverFile(std::string(argv[2]));
  h.importEncodedFile(std::string(argv[1]));
  h.huffmanDecode(argv[3]);
  h.release();
#endif
  return 0;
}


#else

int main(int argc, char** argv){

#ifdef PIPE_STDOUT
  if (argc != 3) {
      std::cout << "\nUsage: arg1->file to encode; arg2->output name(for solver) \n";
      return 0;
  }
  Huffman h;
  h.huffmanEncode(std::string(argv[1]));
  h.exportEncodingHex("huffman_temp");
  h.exportSolverFile(std::string(argv[2])+"_solver");
  h.release();
  std::ifstream ifs;
  ifs.open(std::string("huffman_temp.hex", std::ios::binary));
  if(!ifs.is_open()){
    char c;
    while (ifs.get(c)) {
      std::cout << c;
    }
  }
#else

  if (argc != 3) {
      std::cout << "\nUsage: arg1->file to encode; arg2->output name(without extension) \n";
      return 0;
  }
  Huffman h;
  h.huffmanEncode(std::string(argv[1]));
  h.exportEncodingHex(std::string(argv[2])+"_encoded");
  h.exportSolverFile(std::string(argv[2])+"_solver");
  h.release();
#endif

  return 0;
}

#endif
