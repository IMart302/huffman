#ifndef BITSET_ADAPTER_H
#define BITSET_ADAPTER_H

#include <vector>
#include <fstream>

class BitsetWrapper{
private:
  std::vector<char> wrap;
  std::vector<char>::reverse_iterator last;
  long count;
public:
  BitsetWrapper(){
    count = 0;
    //wrap.push_back(std::bitset<8>(0x00));
  }

  void pushBit(short bit){
    //long prim_index = count / 8;
    long sec_index = count % 8;
    if(bit == 1){
      if(count % 8 == 0){
        wrap.push_back(0xFF);
        last = wrap.rbegin();
      }
      (*last) |= 0x01 << sec_index;
      count++;
    }
    else if(bit == 0){
      if(count % 8 == 0){
        wrap.push_back(0xFF);
        last = wrap.rbegin();
      }
      (*last) &= ~(0x01 << sec_index);
      count++;
    }
  }

  void concatenate(BitsetWrapper &b){
    for(int i = 0; i < b.getCount(); i++){
      this->pushBit(b.getBitLeast(i));
    }
  }

  short getBitLeast(long indx){
    long prim_index = indx / 8;
    long sec_index = indx % 8;
    char c = (wrap[prim_index] >> sec_index) & 0x01;
    if(c == 0x01) return 1;
    else return 0;
    //return wrap[prim_index][sec_index];
  }

  long getCount(){
    return count;
  }

  long numOfBytes(){
    return wrap.size();
  }

  void clear(){
    wrap.clear();
    count = 0;
  }

  void reverse(BitsetWrapper &rev){
    rev.clear();
    for(int i = this->count-1; i>=0; i--){
      rev.pushBit(this->getBitLeast(i));
    }
  }

  void exportToHexFile(std::string filename){
    std::ofstream of;
    //unsigned long n;
    //char c;
    of.open(filename+".hex", std::ios::binary);
    if(of.is_open()){
      for(std::vector<char>::iterator it = wrap.begin(); it!=wrap.end(); ++it){
        of << (*it);
      }
      //for(int i = 0; i < wrap.size(); i++){
      //  //n = wrap[i].to_ulong();
      //  //c = static_cast<char>(n);
      //  of << wrap[i];
      //}
      of.close();
    }
  }

  void importFromHexFile(std::string filename){
    std::ifstream ifs;
    ifs.open(filename, std::ios::binary);
    if(!ifs.is_open()) return;
    wrap.clear();
    char c;
    while(ifs.get(c)){
      wrap.push_back(c);
      count += 8;
    }
    ifs.close();
  }
};

#endif
